var Emitter = require('../../../emitter')
var YBRequest = require('../../request')

var LocationheaderParser = Emitter.extend(
  /** @lends youbora.LocationheaderParser.prototype */
  {

    constructor: function () {
      this._realResource = null
    },

    /**
     * Emits DONE event
     */
    done: function () {
      this.emit(LocationheaderParser.Event.DONE)
    },

    /**
     * Parses given header to check.
     */
    parse: function (url) {
      var request = new YBRequest(url, null, null, {
        method: 'HEAD',
        maxRetries: 0,
        requestHeaders: {},
        cache: true
      })

      request.on(YBRequest.Event.SUCCESS, function (resp) {
        this._realResource = resp.getXHR().responseURL
        this.done()
      }.bind(this))

      request.on(YBRequest.Event.ERROR, function (resp) {
        this.done()
      }.bind(this))

      request.send()
    },

    /**
   * Get the parsed resource. Will be null/undefined if parsing is not yet started
   *
   * @return {string} The parsed resource.
   */
    getResource: function () {
      return this._realResource
    }
  },

  /** @lends youbora.CdnParser */
  {
    // Static members

    /**
     * List of events that could be fired from this class.
     *
     * @enum
     */
    Event: {
      /** Notifies that this CdnParser is done processing. */
      DONE: 'done'
    }
  }
)

module.exports = LocationheaderParser
