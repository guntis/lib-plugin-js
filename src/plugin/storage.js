var YouboraObject = require('../object')
var Log = require('../log')

/**
 * This class manages data sotrage in the browser memory.
 *
 * @extends youbora.Emitter
 * @memberof youbora
 */
var YouboraStorage = YouboraObject.extend(
  /** @lends youbora.YouboraStorage.prototype */
  {
    /**
     *
     * @constructs YouboraStorage
     * @extends YouboraObject
     * @memberof youbora
     *
     * @param {String} [prefix] Optional. Sets the prefix for saved objects in storages or cookies. 'youbora' by default.
     * @param {Boolean} [disableCookies] Optional. Set to true to disable cookies fallback. True by default.
     */
    constructor: function (prefix, disableCookies) {
      this.prefix = prefix || 'youbora'
      this.disableCookies = disableCookies
    },
    /**
     * Saves in localStorage or equivalent
     *
     * @param {string} key Key of the value. Prefix will be appended.
     * @param {string} value Value.
     */
    setLocal: function (key, value) {
      try {
        if (typeof localStorage === 'undefined' || !localStorage) {
          if (!this.disableCookies) {
            return this._setCookie(this.prefix + '.' + key, value)
          }
        } else {
          return localStorage.setItem(this.prefix + '.' + key, value)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs localStorage or cookies, not supported by your browser.')
      }
      return null
    },

    /**
     * Reads from localStorage or equivalent
     *
     * @param {string} key Key of the value. prefix will be appended.
     */
    getLocal: function (key) {
      try {
        if (typeof localStorage === 'undefined' || !localStorage) {
          if (!this.disableCookies) {
            return this._getCookie(this.prefix + '.' + key)
          }
        } else {
          return localStorage.getItem(this.prefix + '.' + key)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs localStorage or cookies, not supported by your browser.')
      }
      return null
    },

    /**
     * Removes from localStorage or equivalent
     *
     * @param {string} key Key of the value. prefix will be appended.
     */
    removeLocal: function (key) {
      try {
        if (typeof localStorage === 'undefined' || !localStorage) {
          if (!this.disableCookies) {
            return this._removeCookie(this.prefix + '.' + key)
          }
        } else {
          return localStorage.removeItem(this.prefix + '.' + key)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs localStorage or cookies, not supported by your browser.')
      }
      return null
    },

    /**
     * Saves in sessionStorage or equivalent
     *
     * @param {string} key Key of the value. prefix will be appended.
     * @param {string} value Value.
     */
    setSession: function (key, value) {
      try {
        if (typeof sessionStorage === 'undefined' || !sessionStorage) {
          if (!this.disableCookies) {
            return this._setCookie(this.prefix + '.' + 'session.' + key, value)
          }
        } else {
          return sessionStorage.setItem(this.prefix + '.' + key, value)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs sessionStorage or cookies, not supported by your browser.')
      }
      return null
    },

    /**
     * Reads from sessionStorage or equivalent
     *
     * @param {string} key Key of the value. prefix will be appended.
     */
    getSession: function (key) {
      try {
        if (typeof sessionStorage === 'undefined' || !sessionStorage) {
          if (!this.disableCookies) {
            return this._getCookie(this.prefix + '.' + 'session.' + key)
          }
        } else {
          return sessionStorage.getItem(this.prefix + '.' + key)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs sessionStorage or cookies, not supported by your browser.')
      }
      return null
    },

    /**
   * Removes from sessionStorage or equivalent
   *
   * @param {string} key Key of the value. prefix will be appended.
   */
    removeSession: function (key) {
      try {
        if (typeof sessionStorage === 'undefined' || !sessionStorage) {
          if (!this.disableCookies) {
            return this._removeCookie(this.prefix + '.' + 'session.' + key)
          }
        } else {
          return sessionStorage.removeItem(this.prefix + '.' + key)
        }
      } catch (err) {
        Log.error('Youbora Infinity needs sessionStorage or cookies, not supported by your browser.')
      }
      return null
    },

    /**
     * Calls getSession and getLocal for the same key
     * @param {string} key Key of the value. prefix will be appended.
     */
    getStorages: function (key) {
      return this.getSession(key) || this.getLocal(key)
    },

    /**
     * Calls getSession and getLocal with the same key and value
     * @param {string} key Key of the value. prefix will be appended.
     * @param {string} value Value.
     */
    setStorages: function (key, value) {
      this.setSession(key, value)
      this.setLocal(key, value)
    },

    /**
     * Calls getSession and getLocal for the same key
     * @param {string} key Key of the value. prefix will be appended.
     */
    removeStorages: function (key) {
      this.removeSession(key)
      this.removeLocal(key)
    },

    // Private cookies methods

    /**
   * Sets a cookie value
   *
   * @param {string} cname Key of the value.
   * @param {Object} cvalue Value.
   */
    _setCookie: function (cname, cvalue) {
      document.cookie = cname + '=' + cvalue + ';'
    },

    /**
   * Gets a cookie value
   *
   * @param {string} cname Key of the value.
   */
    _getCookie: function (cname) {
      var name = cname + '='
      var decodedCookie = decodeURIComponent(document.cookie)
      var ca = decodedCookie.split(';')
      for (var i = 0; i < ca.length; i++) {
        var c = ca[i]
        while (c.charAt(0) === ' ') {
          c = c.substring(1)
        }
        if (c.indexOf(name) === 0) {
          return c.substring(name.length, c.length)
        }
      }
      return null
    },

    /**
   * Removes a cookie
   *
   * @param {string} cname Key of the value.
   */
    _removeCookie: function (cname) {
      document.cookie = cname + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;'
    }
  }
)

module.exports = YouboraStorage
