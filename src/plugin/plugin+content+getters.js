var Log = require('../log')
var Util = require('../util')
var HybridNetwork = require('../monitors/hybridnetwork')
var version = require('../version')

// This file is designed to add extra functionalities to Plugin class

var PluginContentGetterMixin = {
  /**
       * Returns content's playhead
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlayhead: function () {
    var ret = 0
    if (this._adapter) {
      try {
        ret = this._adapter.getPlayhead()
      } catch (err) {
        Log.warn('An error occurred while calling getPlayhead', err)
      }
    }
    return Util.parseNumber(ret, 0)
  },

  /**
       * Returns content's PlayRate
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlayrate: function () {
    var ret = 0
    if (this._adapter) {
      try {
        ret = this._adapter.getPlayrate()
        if (this._adapter.flags && this._adapter.flags.isPaused) ret = 0
      } catch (err) {
        Log.warn('An error occured while calling getPlayrate', err)
      }
    }
    return ret
  },

  /**
       * Returns content's FramesPerSecond
       *
       * @memberof youbora.Plugin.prototype
       */
  getFramesPerSecond: function () {
    var ret = this.options['content.fps']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getFramesPerSecond()
      } catch (err) {
        Log.warn('An error occured while calling getFramesPerSecond', err)
      }
    }
    return ret
  },

  /**
       * Returns content's DroppedFrames
       *
       * @memberof youbora.Plugin.prototype
       */
  getDroppedFrames: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getDroppedFrames()
      } catch (err) {
        Log.warn('An error occured while calling getDroppedFrames', err)
      }
    }
    if (!ret) {
      ret = this.getWebkitDroppedFrames()
    }
    return Util.parseNumber(ret, 0)
  },

  /**
       * Returns dropped frames as per webkitDroppedFrameCount
       *
       * @returns {number}
       *
       * @memberof youbora.Plugin.prototype
       */
  getWebkitDroppedFrames: function () {
    if (this._adapter && this._adapter.tag && this._adapter.tag.webkitDroppedFrameCount) {
      return this._adapter.tag.webkitDroppedFrameCount
    }
    return null
  },

  /**
       * Returns content's Duration
       *
       * @memberof youbora.Plugin.prototype
       */
  getDuration: function () {
    var ret = this.options['content.duration']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getDuration()
      } catch (err) {
        Log.warn('An error occured while calling getDuration', err)
      }
    }
    if (ret === 0) ret = null
    return Util.parseNumber(Math.round(ret), null)
  },

  /**
       * Returns content's Bitrate
       *
       * @memberof youbora.Plugin.prototype
       */
  getBitrate: function () {
    var ret = this.options['content.bitrate']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getBitrate()
      } catch (err) {
        Log.warn('An error occured while calling getBitrate', err)
      }

      if (!ret || ret === -1) {
        ret = this.getWebkitBitrate()
      }
    }
    return Util.parseNumber(ret, -1)
  },

  /**
       * Returns bitrate as per webkitVideoDecodedByteCount
       *
       * @param {Object} tag Video tag DOM reference.
       * @returns {number}
       *
       * @memberof youbora.Plugin.prototype
       */
  getWebkitBitrate: function () {
    if (this._adapter && this._adapter.tag && this._adapter.tag.webkitVideoDecodedByteCount) {
      var bitrate = this._adapter.tag.webkitVideoDecodedByteCount
      if (this._lastWebkitBitrate) {
        var delta = this._adapter.tag.webkitVideoDecodedByteCount - this._lastWebkitBitrate
        bitrate = Math.round(((delta) / this.viewTransform.response.pingTime) * 8)
      }
      this._lastWebkitBitrate = this._adapter.tag.webkitVideoDecodedByteCount
      return bitrate !== 0 ? bitrate : -1
    }
  },

  /**
       * Returns content's Throughput
       *
       * @memberof youbora.Plugin.prototype
       */
  getThroughput: function () {
    var ret = this.options['content.throughput']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getThroughput()
      } catch (err) {
        Log.warn('An error occured while calling getThroughput', err)
      }
    }
    return Util.parseNumber(ret, -1)
  },

  /**
       * Returns content's Rendition
       *
       * @memberof youbora.Plugin.prototype
       */
  getRendition: function () {
    var ret = this.options['content.rendition']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getRendition()
      } catch (err) {
        Log.warn('An error occured while calling getRendition', err)
      }
    }
    return ret
  },

  /**
       * Returns content's Title
       *
       * @memberof youbora.Plugin.prototype
       */
  getTitle: function () {
    var ret = this.options['content.title']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getTitle()
      } catch (err) {
        Log.warn('An error occured while calling getTitle', err)
      }
    }
    return ret
  },

  /**
       * Returns content's Title2
       *
       * @memberof youbora.Plugin.prototype
       */
  getTitle2: function () {
    var ret = this.options['content.program']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getTitle2()
      } catch (err) {
        Log.warn('An error occured while calling getTitle2', err)
      }
    }
    return ret
  },

  /**
       * Returns content's IsLive
       *
       * @memberof youbora.Plugin.prototype
       */
  getIsLive: function () {
    var ret = this.options['content.isLive']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getIsLive()
      } catch (err) {
        Log.warn('An error occured while calling getIsLive', err)
      }
    }
    return ret || false
  },

  /**
       * Returns content's Resource after being parsed by the resourceTransform
       *
       * @memberof youbora.Plugin.prototype
       */
  getResource: function () {
    var ret = null
    if (!this.resourceTransform.isBlocking() && this.resourceTransform.isDone) {
      ret = this.resourceTransform.getResource()
    }

    return ret || this.getOriginalResource()
  },

  /**
       * Returns content's original Resource
       *
       * @memberof youbora.Plugin.prototype
       */
  getOriginalResource: function () {
    var ret = null
    ret = this.options['content.resource']
    if (ret === null && this._adapter) {
      try {
        ret = this._adapter.getResource()
      } catch (err) {
        Log.warn('An error occured while calling getResource', err)
        ret = null
      }
    }
    return ret || null
  },

  /**
       * Returns content's TransactionCode
       *
       * @memberof youbora.Plugin.prototype
       */
  getTransactionCode: function () {
    return this.options['content.transactionCode']
  },

  /**
       * Returns content's Metadata
       *
       * @memberof youbora.Plugin.prototype
       */
  getMetadata: function () {
    return this.options['content.metadata']
  },

  /**
       * Returns content's PlayerVersion
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlayerVersion: function () {
    var ret = ''
    if (this._adapter) {
      try {
        ret = this._adapter.getPlayerVersion()
      } catch (err) {
        Log.warn('An error occured while calling getPlayerVersion', err)
      }
    }
    return ret
  },

  /**
       * Returns content's PlayerName
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlayerName: function () {
    var ret = ''
    if (this._adapter) {
      try {
        ret = this._adapter.getPlayerName()
      } catch (err) {
        Log.warn('An error occured while calling getPlayerName', err)
      }
    }
    return ret
  },

  /**
       * Returns content's Cdn
       *
       * @memberof youbora.Plugin.prototype
       */
  getCdn: function () {
    var ret = null
    if (!this.resourceTransform.isBlocking()) {
      ret = this.resourceTransform.getCdnName()
    }
    return ret || this.options['content.cdn']
  },

  /**
       * Returns content's PluginVersion
       *
       * @memberof youbora.Plugin.prototype
       */
  getPluginVersion: function () {
    var ret = this.getAdapterVersion()
    if (!ret) ret = version + '-adapterless'

    return ret
  },

  /**
       * Returns content's PluginVersion
       *
       * @memberof youbora.Plugin.prototype
       */
  getLibVersion: function () {
    return version
  },

  /**
       * Returns ads adapter getVersion or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getAdapterVersion: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getVersion()
      } catch (err) {
        Log.warn('An error occured while calling getPluginVersion', err)
      }
    }
    return ret
  },

  /**
       * Returns cdn traffic received in bytes or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getCdnTraffic: function () {
    var ret = HybridNetwork.getCdnTraffic()
    if (this._adapter) {
      try {
        ret = this._adapter.getCdnTraffic() || ret
      } catch (err) {
        Log.warn('An error occured while calling getCdnTraffic', err)
      }
    }
    return ret
  },

  /**
       * Returns p2p traffic received in bytes or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getP2PTraffic: function () {
    var ret = HybridNetwork.getP2PTraffic()
    if (this._adapter) {
      try {
        ret = this._adapter.getP2PTraffic() || ret
      } catch (err) {
        Log.warn('An error occured while calling getP2PTraffic', err)
      }
    }
    return ret
  },

  /**
       * Returns p2p traffic sent in bytes or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getUploadTraffic: function () {
    var ret = HybridNetwork.getUploadTraffic()
    if (this._adapter) {
      try {
        ret = this._adapter.getUploadTraffic() || ret
      } catch (err) {
        Log.warn('An error occured while calling getUploadTraffic', err)
      }
    }
    return ret
  },

  /**
       * Returns if p2p plugin is enabled or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getIsP2PEnabled: function () {
    var ret = HybridNetwork.getIsP2PEnabled()
    if (this._adapter) {
      try {
        ret = this._adapter.getIsP2PEnabled() || ret
      } catch (err) {
        Log.warn('An error occured while calling getIsP2PEnabled', err)
      }
    }
    return ret
  },

  getStreamingProtocol: function () {
    var ret = this.options['content.streamingProtocol']
    if (typeof ret !== 'string') return null
    ret = ret.toUpperCase()
    if (ret !== 'HDS' && ret !== 'HLS' && ret !== 'MSS' && ret !== 'DASH' && ret !== 'RTMP' && ret !== 'RTP' && ret !== 'RTSP') {
      Log.warn('Streaming protocol ' + ret + ' is not a valid value')
      return null
    }
    return ret
  },

  /** Returns household id */
  getHouseholdId: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getHouseholdId()
      } catch (err) {
        Log.warn('An error occurred while calling getHouseholdId', err)
      }
    }
    return ret
  },

  /**
       * Returns latency of a live video, or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getLatency: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getLatency()
      } catch (err) {
        Log.warn('An error occurred while calling getLatency', err)
      }
    }
    return ret
  },

  /**
       * Returns the amount of packets lost, or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getPacketLoss: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getPacketLoss()
      } catch (err) {
        Log.warn('An error occurred while calling getPacketLoss', err)
      }
    }
    return ret
  },

  /**
       * Returns the amount of packets sent, or null
       *
       * @memberof youbora.Plugin.prototype
       */
  getPacketSent: function () {
    var ret = null
    if (this._adapter) {
      try {
        ret = this._adapter.getPacketSent()
      } catch (err) {
        Log.warn('An error occurred while calling getPacketSent', err)
      }
    }
    return ret
  },

  /**
       * Returns a json with the metrics to be reported in pings when changed
       *
       * @memberof youbora.Plugin.prototype
       */
  getVideoMetrics: function () {
    var metrics = this._adapter ? this._adapter.getMetrics() : null
    metrics = metrics || this.options['content.metrics']
    for (var metric in metrics) {
      if (typeof metrics[metric] !== 'object' || !metrics[metric].value) {
        var temporal = {}
        // temporal.oper = 'SUM' //default one?
        temporal.value = metrics[metric]
        metrics[metric] = temporal
      }
    }
    return metrics
  },
  // ----------------------------------------- CHRONOS ------------------------------------------

  /**
       * Returns JoinDuration chrono delta time
       *
       * @memberof youbora.Plugin.prototype
       */
  getJoinDuration: function () {
    return this._adapter ? this._adapter.chronos.join.getDeltaTime(false) : -1
  },

  /**
       * Returns BufferDuration chrono delta time
       *
       * @memberof youbora.Plugin.prototype
       */
  getBufferDuration: function () {
    return this._adapter ? this._adapter.chronos.buffer.getDeltaTime(false) : -1
  },

  /**
       * Returns SeekDuration chrono delta time
       *
       * @memberof youbora.Plugin.prototype
       */
  getSeekDuration: function () {
    return this._adapter ? this._adapter.chronos.seek.getDeltaTime(false) : -1
  },

  /**
       * Returns pauseDuration chrono delta time
       *
       * @memberof youbora.Plugin.prototype
       */
  getPauseDuration: function () {
    return this._adapter ? this._adapter.chronos.pause.getDeltaTime(false) : 0
  },

  /**
       * Returns content's package
       *
       * @memberof youbora.Plugin.prototype
       */
  getPackage: function () {
    return this.options['content.package']
  },

  /**
       * Returns content's saga
       *
       * @memberof youbora.Plugin.prototype
       */
  getSaga: function () {
    return this.options['content.saga']
  },

  /**
       * Returns content's tv show
       *
       * @memberof youbora.Plugin.prototype
       */
  getTvShow: function () {
    return this.options['content.tvShow']
  },

  /**
       * Returns content's season
       *
       * @memberof youbora.Plugin.prototype
       */
  getSeason: function () {
    return this.options['content.season']
  },

  /**
       * Returns content's episode title
       *
       * @memberof youbora.Plugin.prototype
       */
  getEpisodeTitle: function () {
    return this.options['content.episodeTitle']
  },

  /**
       * Returns content's channel
       *
       * @memberof youbora.Plugin.prototype
       */
  getChannel: function () {
    return this.options['content.channel']
  },

  /**
       * Returns content's id
       *
       * @memberof youbora.Plugin.prototype
       */
  getID: function () {
    return this.options['content.id']
  },

  /**
       * Returns content's IMDB id
       *
       * @memberof youbora.Plugin.prototype
       */
  getImdbId: function () {
    return this.options['content.imdbId']
  },

  /**
       * Returns content's gracenote id
       *
       * @memberof youbora.Plugin.prototype
       */
  getGracenoteID: function () {
    return this.options['content.gracenoteId']
  },

  /**
       * Returns content's type
       *
       * @memberof youbora.Plugin.prototype
       */
  getType: function () {
    return this.options['content.type']
  },

  /**
       * Returns content's genre
       *
       * @memberof youbora.Plugin.prototype
       */
  getGenre: function () {
    return this.options['content.genre']
  },

  /**
       * Returns content's language
       *
       * @memberof youbora.Plugin.prototype
       */
  getVideoLanguage: function () {
    return this.options['content.language']
  },

  /**
       * Returns content's subtitles
       *
       * @memberof youbora.Plugin.prototype
       */
  getSubtitles: function () {
    return this.options['content.subtitles']
  },

  /**
       * Returns content's contracted resolution
       *
       * @memberof youbora.Plugin.prototype
       */
  getContractedResolution: function () {
    return this.options['content.contractedResolution']
  },

  /**
       * Returns content's cost
       *
       * @memberof youbora.Plugin.prototype
       */
  getCost: function () {
    return this.options['content.cost']
  },

  /**
       * Returns content's price
       *
       * @memberof youbora.Plugin.prototype
       */
  getPrice: function () {
    return this.options['content.price']
  },

  /**
       * Returns content's playback type
       *
       * @memberof youbora.Plugin.prototype
       */
  getPlaybackType: function () {
    if (this.options['content.playbackType']) {
      return this.options['content.playbackType']
    }
    if (this.getIsLive()) {
      return 'Live'
    }
    return 'VoD'
  },

  /**
       * Returns content's DRM
       *
       * @memberof youbora.Plugin.prototype
       */
  getDRM: function () {
    return this.options['content.drm']
  },

  /**
       * Returns content's video codec
       *
       * @memberof youbora.Plugin.prototype
       */
  getVideoCodec: function () {
    return this.options['content.encoding.videoCodec']
  },

  /**
       * Returns content's audio codec
       *
       * @memberof youbora.Plugin.prototype
       */
  getAudioCodec: function () {
    return this.options['content.encoding.audioCodec']
  },

  /**
       * Returns content's codec settings
       *
       * @memberof youbora.Plugin.prototype
       */
  getCodecSettings: function () {
    return this.options['content.encoding.codecSettings']
  },

  /**
       * Returns content's codec profile
       *
       * @memberof youbora.Plugin.prototype
       */
  getCodecProfile: function () {
    return this.options['content.encoding.codecProfile']
  },

  /**
       * Returns content's container format
       *
       * @memberof youbora.Plugin.prototype
       */
  getContainerFormat: function () {
    return this.options['content.encoding.containerFormat']
  }
}

module.exports = PluginContentGetterMixin
