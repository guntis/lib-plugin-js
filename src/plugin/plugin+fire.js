var Log = require('../log')
var Util = require('../util')
var Constants = require('../constants')
var Transform = require('../comm/transform/transform')

// This file is designed to add extra functionalities to Plugin class

/** @lends youbora.Plugin.prototype */
var PluginFireMixin = {
  /**
   * Sends /init. Should be called once the user has requested the content. Does not need
   * a working adapter or player to work. it won't sent start if isInitiated is true.
   *
   * @param {Object} [params] Object of key:value params.
   *
   * @memberof youbora.Plugin.prototype
   */
  fireInit: function (params) {
    if (!this.isInitiated) {
      if (!this.getAdapter() || (this.getAdapter() && !this.getAdapter().flags.isStarted)) {
        this.viewTransform.nextView()
        this._initComm()
        this._startPings()
        this.initChrono.start()
        this.isInitiated = true

        params = params || {}
        this._send(Constants.WillSendEvent.WILL_SEND_INIT, Constants.Service.INIT, params)
        this._adSavedError()
        this._adSavedManifest()
        Log.notice(Constants.Service.INIT + ' ' + (params.title || params.mediaResource))
      }
    }
  },

  /**
   * Sends /error. Should be used when the error is related to out-of-player errors: like async
   * resource load or player loading errors.
   *
   * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
   * @param {String} [msg] Error Message
   * @param {Object} [metadata] Object defining error metadata
   * @param {String} [level] Level of the error. Currently supports 'error' and 'fatal'
   *
   * @memberof youbora.Plugin.prototype
   */
  fireError: function (object, msg, metadata, level) {
    if (!this.isInitiated && (!this.getAdapter() || !this.getAdapter().flags.isStarted)) this.viewTransform.nextView()
    if (!this._comm) this._initComm()
    var params = Util.buildErrorParams(object, msg, metadata, level)
    if (params.code) {
      delete params.code
    }
    this._send(Constants.WillSendEvent.WILL_SEND_ERROR, Constants.Service.ERROR, params)
    this._adSavedError()
    this._adSavedManifest()
    Log.notice(Constants.Service.ERROR +
      ' ' + params.errorLevel +
      ' ' + params.errorCode
    )

    if (params.errorLevel === 'fatal') {
      this.fireStop()
    }
  },

  /**
   * Calls fireErrors and then stops pings.
   *
   * @param {String|Object} [code] Error Code, if an object is sent, it will be treated as params.
   * @param {String} [msg] Error Message
   * @param {Object} [metadata] Object defining error metadata
   *
   * @memberof youbora.Plugin.prototype
   */
  fireFatalError: function (object, msg, metadata, level) {
    this.fireError(object, msg, metadata, level)
    this.fireStop()
  },

  /**
   * Fires /stop. Should be used to terminate sessions once the player is gone or if
   * plugin.fireError() is called.
   *
   * @param {Object} [params] Object of key:value params.
   *
   * @memberof youbora.Plugin.prototype
   */
  fireStop: function (params) {
    if (this.isInitiated || this.isStarted) {
      if (this._adapter) {
        this._adapter.flags.isStopped = true
      }
      if (this._adsAdapter && this.isBreakStarted) {
        this._adsAdapter.fireBreakStop()
      }
      params = params || {}
      this._send(Constants.WillSendEvent.WILL_SEND_STOP, Constants.Service.STOP, params)
      Log.notice(Constants.Service.STOP + ' at ' + params.playhead + 's')
      this._reset()
    }
  },

  /**
   * Fires /offlineEvents. If offline is disabled, will try to send all the views stored.
   *
   * @param {Object} [params] Object of key:value params.
   *
   * @memberof youbora.Plugin.prototype
   */
  fireOfflineEvents: function (params) {
    if (this.options && !this.options['offline']) {
      if (!this.isInitiated &&
        (!this._adapter || !this._adapter.flags.isStarted) &&
        (!this._adsAdapter || !this._adsAdapter.flags.isStarted)) {
        this._offlineParams = params
        if (this.viewTransform.response.code && this.viewTransform.response.host) {
          this._generateAndSendOffline()
        } else {
          this.offlineReference = this._generateAndSendOffline.bind(this)
          this.viewTransform.on(Transform.Event.DONE, this.offlineReference)
        }
      } else {
        Log.error('Adapters have to be stopped')
      }
    } else {
      Log.error('To send offline events, offline option must be disabled')
    }
  },

  _generateAndSendOffline: function () {
    var params = this._offlineParams
    this._initComm()
    while (true) {
      var bodyAndId = this.requestBuilder.buildBody(Constants.Service.OFFLINE_EVENTS).viewJson
      if (bodyAndId[0] === null) break
      var newViewCode = this.viewTransform.nextView()
      var body = bodyAndId[0].replace(/CODE_PLACEHOLDER/g, newViewCode.toString())
        .replace(/,"sessionId":"SESSION_PLACEHOLDER"/g, '') // this.viewTransform.getSessionId()
        .replace(/,"sessionRoot":"ROOT_PLACEHOLDER"/g, '') // this.viewTransform.getSessionRoot()
        .replace(/,"parentId":"PARENT_PLACEHOLDER"/g, '') // this.viewTransform.getParentId()
      // modify to support offline+infinity
      this._send(Constants.WillSendEvent.WILL_SEND_OFFLINE_EVENTS, Constants.Service.OFFLINE_EVENTS,
        params, body, 'POST', function (a, callbackParams) {
          this.offlineStorage.removeView(callbackParams['offlineId'])
        }.bind(this), { 'offlineId': bodyAndId[1] })
    }
    this.offlineStorage.sent()
    this._offlineParams = null
  }
}

module.exports = PluginFireMixin
