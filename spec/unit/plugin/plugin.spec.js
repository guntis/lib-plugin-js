describe('Plugin', () => {
  var Plugin = require('../../../src/plugin/plugin')
  var plugin
  var adapter = {
    id: 'adapter',
    on: function () { },
    off: function () { },
    dispose: function () { },
    getMetrics: function () { },
    flags: {
      isStarted: false,
      isJoined: false,
      isPaused: false,
      isSeeking: false,
      isBuffering: false,
      isEnded: false,
      isStopped: false
    }
  }

  beforeEach(() => {
    plugin = new Plugin()
  })

  it('should declare options', () => {
    plugin.setOptions({ accountCode: 'a' })
    expect(plugin.options.accountCode).toBe('a')
  })

  it('should add and remove adapters', () => {
    spyOn(adapter, 'on')
    spyOn(adapter, 'off')
    spyOn(adapter, 'dispose')
    adapter.plugin = null
    plugin.setAdapter(adapter)
    expect(adapter.on).toHaveBeenCalled()
    expect(plugin.getAdapter().id).toBe('adapter')
    plugin.removeAdapter()
    expect(adapter.off).toHaveBeenCalled()
    expect(adapter.dispose).toHaveBeenCalled()
    expect(plugin.getAdapter()).toBe(null)
  })

  it('should not add adapter to more than 1 plugin', () => {
    var plugin2 = new Plugin()

    adapter.plugin = null
    plugin.setAdapter(adapter)
    plugin2.setAdapter(adapter)
    expect(plugin2.getAdapter()).toBe(null)

    adapter.plugin = null
    plugin.setAdsAdapter(adapter)
    plugin2.setAdsAdapter(adapter)
    expect(plugin2.getAdsAdapter()).toBe(null)
  })

  it('should add and remove Ads Adapters', () => {
    spyOn(adapter, 'on')
    spyOn(adapter, 'off')
    spyOn(adapter, 'dispose')
    adapter.plugin = null
    plugin.setAdsAdapter(adapter)
    expect(adapter.on).toHaveBeenCalled()
    expect(plugin.getAdsAdapter().id).toBe('adapter')
    plugin.removeAdsAdapter()
    expect(adapter.off).toHaveBeenCalled()
    expect(adapter.dispose).toHaveBeenCalled()
    expect(plugin.getAdsAdapter()).toBe(null)
  })

  it('should disable/enable', () => {
    plugin.disable()
    expect(plugin.options.enabled).toBe(false)
    plugin.enable()
    expect(plugin.options.enabled).toBe(true)
  })

  it('should init', () => {
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireInit()
    expect(plugin.isInitiated).toBe(true)
    expect(plugin.viewTransform.nextView).toHaveBeenCalled()
  })

  it('should not init, with adapter started', () => {
    plugin.getAdapter = function () {
      return {
        flags: {
          isStarted: true
        }
      }
    }
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireInit()
    expect(plugin.isInitiated).toBe(false)
    expect(plugin.viewTransform.nextView).not.toHaveBeenCalled()
  })

  it('should not init if already inited', () => {
    plugin.isInitiated = true
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireInit()
    expect(plugin.isInitiated).toBe(true)
    expect(plugin.viewTransform.nextView).not.toHaveBeenCalled()
  })

  it('should return comm', () => {
    expect(plugin.getComm()).toBeUndefined()
    plugin._initComm()
    expect(plugin.getComm()).toBeDefined()
  })

  it('should send fatal error', () => {
    spyOn(plugin, 'fireError')
    spyOn(plugin, 'fireStop')
    plugin.fireFatalError()
    expect(plugin.fireError).toHaveBeenCalled()
    expect(plugin.fireStop).toHaveBeenCalled()
  })

  it('should send error with new view', () => {
    spyOn(plugin, '_send')
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireError()
    expect(plugin._send).toHaveBeenCalled()
    expect(plugin.viewTransform.nextView).toHaveBeenCalled()
  })

  it('should send error with previous view', () => {
    plugin.isInitiated = true
    spyOn(plugin, '_send')
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireError()
    expect(plugin._send).toHaveBeenCalled()
    expect(plugin.viewTransform.nextView).not.toHaveBeenCalled()
  })

  it('should send error and create new view', () => {
    spyOn(plugin, '_send')
    spyOn(plugin.viewTransform, 'nextView')
    plugin.fireError({ errorLevel: 'fatal' })
    expect(plugin._send).toHaveBeenCalled()
    expect(plugin.viewTransform.nextView).toHaveBeenCalled()
  })

  it('should store data response properly', () => {
    plugin._receiveData({
      target: {
        response: {
          pingTime: 6,
          beatTime: 40,
          sessionExpire: 200,
          msg: 'test123'
        }
      }
    })
    expect(plugin._ping.interval).toBe(6000)
    expect(plugin._beat.interval).toBe(40000)
    expect(plugin.sessionExpire).toBe(200000)
    expect(plugin.storage.getLocal('data')).toBe('test123')
    expect(plugin.storage.getSession('data')).toBe('test123')
  })

  it('should block fireoffline event if offline is enabled', () => {
    plugin.options['offline'] = true
    spyOn(plugin, '_generateAndSendOffline')
    plugin.fireOfflineEvents()
    expect(plugin._generateAndSendOffline).not.toHaveBeenCalled()
  })

  it('should not block fireoffline event if offline is disabled', () => {
    plugin.options['offline'] = false
    plugin.viewTransform.response.code = 'testcode'
    plugin.viewTransform.response.host = 'testhost'
    spyOn(plugin, '_generateAndSendOffline')
    plugin.fireOfflineEvents()
    expect(plugin._generateAndSendOffline).toHaveBeenCalled()
  })

  it('should send offline events', () => {
    spyOn(plugin.requestBuilder, 'buildBody').and.callThrough()
    plugin.options['offline'] = false
    plugin.viewTransform.response.code = 'testcode'
    plugin.viewTransform.response.host = 'testhost'
    plugin.fireOfflineEvents()
    expect(plugin.requestBuilder.buildBody).toHaveBeenCalled()
  })

  it('should search the expected params to send in pause', () => {
    adapter.flags.isPaused = true
    adapter.plugin = null
    plugin.setAdapter(adapter)
    spyOn(plugin, 'getPauseDuration')
    plugin._sendPing()
    expect(plugin.getPauseDuration).toHaveBeenCalled()
  })

  it('should search the expected params to send while playing', () => {
    adapter.flags.isPaused = false
    adapter.plugin = null
    plugin.setAdapter(adapter)
    spyOn(plugin, 'getBitrate')
    spyOn(plugin, 'getThroughput')
    spyOn(plugin, 'getFramesPerSecond')
    plugin._sendPing()
    expect(plugin.getBitrate).toHaveBeenCalled()
    expect(plugin.getThroughput).toHaveBeenCalled()
    expect(plugin.getFramesPerSecond).toHaveBeenCalled()
  })

  it('should search the expected params to send after jointime', () => {
    adapter.flags.isJoined = true
    adapter.plugin = null
    plugin.setAdapter(adapter)
    spyOn(plugin, 'getPlayhead')
    plugin._sendPing()
    expect(plugin.getPlayhead).toHaveBeenCalled()
  })

  it('should search the expected params to send while seeking', () => {
    adapter.flags.isSeeking = true
    adapter.plugin = null
    plugin.setAdapter(adapter)
    spyOn(plugin, 'getSeekDuration')
    plugin._sendPing()
    expect(plugin.getSeekDuration).toHaveBeenCalled()
  })

  it('should search the expected params to send while buffering', () => {
    adapter.flags.isSeeking = false
    adapter.flags.isBuffering = true
    adapter.plugin = null
    plugin.setAdapter(adapter)
    spyOn(plugin, 'getBufferDuration')
    plugin._sendPing()
    expect(plugin.getBufferDuration).toHaveBeenCalled()
  })

  it('should search the expected params to ad starts', () => {
    var adapter2 = {
      id: 'adapter',
      on: function () { },
      off: function () { },
      dispose: function () { },
      getMetrics: function () { },
      flags: {
        isStarted: false,
        isJoined: false,
        isPaused: false,
        isSeeking: false,
        isBuffering: false,
        isEnded: false,
        isStopped: false
      }
    }
    adapter.flags.isStarted = true
    adapter.flags.isBuffering = false
    adapter.plugin = null
    adapter2.flags.isStarted = true
    adapter2.plugin = null
    plugin.setAdapter(adapter2)
    plugin.setAdsAdapter(adapter)
    spyOn(plugin, 'getAdPlayhead')
    spyOn(plugin, 'getAdViewedDuration')
    spyOn(plugin, 'getAdViewability')
    spyOn(plugin, 'getAdBitrate')
    plugin._sendPing()
    expect(plugin.getAdPlayhead).toHaveBeenCalled()
    expect(plugin.getAdViewedDuration).toHaveBeenCalled()
    expect(plugin.getAdViewability).toHaveBeenCalled()
    expect(plugin.getAdBitrate).toHaveBeenCalled()
  })
  it('should set the adapter', () => {
    var plugin2 = new Plugin({}, {
      on: () => { },
      off: () => { },
      confirm: true
    })
    expect(plugin2.getAdapter().confirm).toBeTruthy()
  })

  it('should request a new data, no tracking started', () => {
    spyOn(plugin, 'restartViewTransform')
    plugin._checkOldData()
    expect(plugin.restartViewTransform).toHaveBeenCalled()
  })

  it('should not request a new data, video tracking started', () => {
    spyOn(plugin, 'restartViewTransform')
    plugin._adapter = {
      flags: {
        isStarted: true
      }
    }
    plugin._checkOldData()
    expect(plugin.restartViewTransform).not.toHaveBeenCalled()
  })

  it('should not request a new data, session tracking started', () => {
    spyOn(plugin, 'restartViewTransform')
    plugin.infinity = {
      infinityStarted: true,
      infinityStopped: false
    }
    plugin._checkOldData()
    expect(plugin.restartViewTransform).not.toHaveBeenCalled()
  })

  it('should parse data', () => {
    var data = {
      target: {
        response: {
          pingTime: 5,
          beatTime: 30,
          sessionExpire: 3600,
          msg: 'message',
          code: 'code'
        }
      }
    }
    spyOn(plugin.viewTransform, 'setSessionId')
    plugin._receiveData(data)
    expect(plugin.sessionExpire).toBe(3600000)
    expect(plugin._ping.interval).toBe(5000)
    expect(plugin._beat.interval).toBe(30000)
    expect(plugin.viewTransform.setSessionId).toHaveBeenCalled()
  })

  it('should parse data, session expired', () => {
    var data = {
      target: {
        response: {
          pingTime: 5,
          beatTime: 30,
          sessionExpire: 3600,
          msg: 'message',
          code: 'code'
        }
      }
    }
    plugin.getIsSessionExpired = function () {
      return false
    }
    spyOn(plugin.viewTransform, 'setSessionId')
    plugin._receiveData(data)
    expect(plugin.viewTransform.setSessionId).toHaveBeenCalled()
  })

  it('should enable and disable background', () => {
    var plugin2 = new Plugin({ 'background.enabled': true })
    expect(plugin2.backgroundDetector.isBackgroundDetectorStarted).toBeTruthy()
    plugin2.setOptions({ 'background.enabled': false })
    expect(plugin2.backgroundDetector.isBackgroundDetectorStarted).toBeFalsy()
  })

  it('should disable and enable background', () => {
    var plugin2 = new Plugin({ 'background.enabled': false })
    expect(plugin2.backgroundDetector.isBackgroundDetectorStarted).toBeFalsy()
    plugin2.setOptions({ 'background.enabled': true })
    expect(plugin2.backgroundDetector.isBackgroundDetectorStarted).toBeTruthy()
  })
})
