describe('HybridNetwork', () => {
  var hybridNetwork = require('../../../src/monitors/hybridnetwork')

  it('should get no CDN traffic', () => {
    Streamroot = undefined
    peer5 = undefined
    expect(hybridNetwork.getCdnTraffic()).toBeNull()
  })

  it('should get no P2P traffic', () => {
    Streamroot = undefined
    peer5 = undefined
    expect(hybridNetwork.getP2PTraffic()).toBeNull()
  })

  it('should get no upload traffic', () => {
    Streamroot = undefined
    peer5 = undefined
    expect(hybridNetwork.getUploadTraffic()).toBeNull()
  })

  it('should get CDN traffic value for streamroot', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      peerAgents: [
        {
          stats: {
            isP2PEnabled: true,
            cdn: 100,
            p2p: 100,
            upload: 100
          }
        }, {
          stats: {
            isP2PEnabled: false,
            cdn: 200,
            p2p: 200,
            upload: 200,
          }
        }
      ]
    }
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
  })

  it('should get P2P traffic value for streamroot', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      peerAgents: [
        {
          isP2PEnabled: true,
          stats: {
            p2p: 100
          }
        }, {
          isP2PEnabled: false,
          stats: {
            p2p: 200
          }
        }
      ]
    }
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
  })

  it('should get Upload traffic value for streamroot', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      peerAgents: [
        {
          isP2PEnabled: true,
          stats: {
            upload: 200
          }
        }, {
          isP2PEnabled: false,
          stats: {
            upload: 100
          }
        }
      ]
    }
    expect(hybridNetwork.getUploadTraffic()).toBe(200)
  })

  it('should get Upload traffic value for streamroot', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      peerAgents: [
        {
          isP2PEnabled: true
        }, {
          isP2PEnabled: false
        }
      ]
    }
    expect(hybridNetwork.getIsP2PEnabled()).toBeTruthy()
  })

  it('should get CDN traffic value for peer5', () => {
    Streamroot = undefined
    peer5 = {}
    peer5.getStats = function () {
      var object = {
        totalHttpDownloaded: 300
      }
      return object
    }
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
  })

  it('should get P2P traffic value for peer5', () => {
    Streamroot = undefined
    peer5 = {}
    peer5.getStats = function () {
      var object = {
        totalP2PDownloaded: 100
      }
      return object
    }
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
  })

  it('should get Upload traffic value for peer5', () => {
    Streamroot = undefined
    peer5 = {}
    peer5.getStats = function () {
      var object = {
        totalP2PUploaded: 200
      }
      return object
    }
    expect(hybridNetwork.getUploadTraffic()).toBe(200)
  })

  it('should get Upload traffic value for peer5', () => {
    Streamroot = undefined
    peer5 = {}
    peer5.isEnabled = function () {
      return true
    }
    expect(hybridNetwork.getIsP2PEnabled()).toBeTruthy()
  })

  it('should get CDN traffic value for teltoo', () => {
    Streamroot = undefined
    teltoo = {}
    teltoo.getStats = function () {
      var object = {
        totalReceivedBytes: 600,
        p2pReceivedBytes: 300
      }
      return object
    }
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
  })

  it('should get P2P traffic value for teltoo', () => {
    Streamroot = undefined
    teltoo = {}
    teltoo.getStats = function () {
      var object = {
        totalReceivedBytes: 600,
        p2pReceivedBytes: 100
      }
      return object
    }
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
  })

  // New streamroot

  it('should get CDN traffic value for streamroot 2', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      instances: [
        {
          dnaDownloadEnabled: true,
          dnaUploadEnabled: true,
          stats: {
            currentContent: {
              cdnDownload: 300,
              dnaDownload: 100,
              dnaUpload: 100
            }
          }
        }
      ]
    }
    expect(hybridNetwork.getCdnTraffic()).toBe(300)
  })

  it('should get P2P traffic value for streamroot 2', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      instances: [
        {
          dnaDownloadEnabled: true,
          dnaUploadEnabled: true,
          stats: {
            currentContent: {
              cdnDownload: 300,
              dnaDownload: 100,
              dnaUpload: 100
            }
          }
        }
      ]
    }
    expect(hybridNetwork.getP2PTraffic()).toBe(100)
  })

  it('should get Upload traffic value for streamroot 2', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      instances: [
        {
          dnaDownloadEnabled: true,
          dnaUploadEnabled: true,
          stats: {
            currentContent: {
              cdnDownload: 300,
              dnaDownload: 100,
              dnaUpload: 100
            }
          }
        }
      ]
    }
    expect(hybridNetwork.getUploadTraffic()).toBe(100)
  })

  it('should get Upload traffic value for streamroot 2', () => {
    peer5 = undefined
    Streamroot = {
      p2pAvailable: true,
      instances: [
        {
          dnaDownloadEnabled: true,
          dnaUploadEnabled: true,
          stats: {
            currentContent: {
              cdnDownload: 300,
              dnaDownload: 100,
              dnaUpload: 100
            }
          }
        }
      ]
    }
    expect(hybridNetwork.getIsP2PEnabled()).toBeTruthy()
  })
})
